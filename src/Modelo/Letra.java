/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Lab05pc05
 */
public class Letra {
    
    private char letra;

    public char getLetra() {
        return letra;
    }

    public void setLetra(char letra) {
        this.letra = letra;
    }

    public Letra() {
    }

    public Letra(char letra) {
        this.letra = letra;
    }
    @Override
    public String toString() {
        return "Letra{" + "letra=" + letra + '}';
    }
    
    
    
}
